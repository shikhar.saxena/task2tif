import { useFormik } from "formik";
import * as Yup from "yup";
import { FormContext } from "./providers/FormState";
import { useContext, useEffect } from "react";
import {
  Box,
  SimpleGrid,
  FormLabel,
  Input,
  Flex,
  Button,
  Spacer,
} from "@chakra-ui/react";

function InterviewDetails({ next, tabIndex, previous }) {
  const { state, dispatch } = useContext(FormContext);
  const MultiformSchema = Yup.object({
    InterviewMod: Yup.string().required("Interview Mode is Required"),
    InterviewDur: Yup.string().required("Interview Duration is Required"),
    InterviewLan: Yup.string().required("Interview Language is Required"),
  });

  const formik = useFormik({
    initialValues: {
      InterviewMod:
        state.interviewDetails.length > 0
          ? `${state.interviewDetails[0].interviewMode}`
          : "",
      InterviewDur:
        state.interviewDetails.length > 0
          ? `${state.interviewDetails[0].interviewDuration}`
          : "",
      InterviewLan:
        state.interviewDetails.length > 0
          ? `${state.interviewDetails[0].interviewLang}`
          : "",
    },

    validationSchema: MultiformSchema,
    onSubmit: (values) => {
      //
      next();
      console.log(values);
    },
  });
  useEffect(() => {
    const newInterviewDetails = {
      interviewMode: formik.values.InterviewMod,
      interviewDuration: formik.values.InterviewDur,
      interviewLang: formik.values.InterviewLan,
    };

    dispatch({ type: "ADD_INTERVIEW", payload: newInterviewDetails });
  }, [formik.values]);

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <SimpleGrid columns={1} spacing={3} alignItems="stretch">
          <Box paddingBottom={2}>
            <FormLabel>Interview Mode</FormLabel>
            <Input
              name="InterviewMod"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.InterviewMod}
            />
            {formik.errors.InterviewMod && formik.touched.InterviewMod ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.InterviewMod}
              </p>
            ) : null}
          </Box>
          <Box paddingBottom={2}>
            <FormLabel>Interview Duration</FormLabel>
            <Input
              name="InterviewDur"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.InterviewDur}
            />
            {formik.errors.InterviewDur && formik.touched.InterviewDur ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.InterviewDur}
              </p>
            ) : null}
          </Box>
          <Box paddingBottom={5}>
            <FormLabel>Interview Language</FormLabel>
            <Input
              name="InterviewLan"
              size="lg"
              bg="white"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.InterviewLan}
            />
            {formik.errors.InterviewLan && formik.touched.InterviewLan ? (
              <p style={{ color: "red", textAlign: "left" }}>
                {formik.errors.InterviewLan}
              </p>
            ) : null}
          </Box>
        </SimpleGrid>

        <Flex justify="center" alignItems="center" margin={5}>
          <Box>
            <Button
              bg="gray.200"
              color="black"
              value={tabIndex}
              onClick={previous}
            >
              Previous
            </Button>
          </Box>
          <Spacer />
          <Box>
            <Button type="submit" bg="#F87671" color="white" value={tabIndex}>
              {tabIndex == 2 ? "Submit" : "Next"}
            </Button>
          </Box>
        </Flex>
      </form>
    </>
  );
}

export default InterviewDetails;
