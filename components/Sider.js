import { Box, Flex, SimpleGrid, Spacer, Tag } from "@chakra-ui/react";
import React, { useContext } from "react";
import { FormContext } from "./providers/FormState";
function Sider() {
  const { state } = useContext(FormContext);
  // console.log(requisitionDetails);
  // console.log(response);
  return (
    <>
      <Flex>
        <Box>
          <b>
            <i>Draft</i>
          </b>
        </Box>
        <Spacer />
        <Box>
          <Tag p={3}>Preview</Tag>
        </Box>
      </Flex>

      <Flex
        bg="#253754"
        marginTop="50px"
        paddingBottom="25px"
        paddingTop="25px"
        paddingRight="10px"
        paddingLeft="10px"
        borderRadius="12px"
        boxShadow="xl"
      >
        <Box alignItems="center" justifyContent="center" color="white">
          Job title:{" "}
          {state.response.length > 0 ? (
            <b>{state.response[0].jobTitle}</b>
          ) : null}
        </Box>
        <Spacer />
        <Box color="white">
          <b>
            <h1>
              OPENINGS:{" "}
              {state.requisitionDetails.length > 0 ? (
                <b>{state.requisitionDetails[0].openings}</b>
              ) : null}
            </h1>
          </b>
        </Box>
      </Flex>
      <Box bg="white" marginTop="50px" padding="3" borderRadius="10px">
        <Flex>
          <Box>
            <b>REQUISITIONS DETAILS</b>
          </Box>
        </Flex>

        <SimpleGrid
          spacingX={15}
          marginTop="40PX"
          fontSize={10}
          textStyle="bold"
          columns={3}
          justifyItems="flex-start"
        >
          <Box>EMPLOYMENT TYPE</Box>

          <Box>OWNER</Box>

          <Box>URGENCY</Box>
        </SimpleGrid>

        <SimpleGrid
          marginTop="1PX"
          spacingX={15}
          columns={3}
          justifyItems="flex-start"
        >
          <Box>
            <b>Permanent</b>
          </Box>
          <Box>
            <b>
              {state.requisitionDetails.length > 0 ? (
                <b>{state.requisitionDetails[0].owner}</b>
              ) : null}{" "}
            </b>
          </Box>
          <Box>
            <b>
              {state.requisitionDetails.length > 0 ? (
                <b>{state.requisitionDetails[0].urgency}</b>
              ) : null}
            </b>
          </Box>
        </SimpleGrid>

        <Flex marginTop="50PX" fontSize={14}>
          <Box>Gender Preference</Box>
        </Flex>
        <Flex>
          <Box>
            <b>No Preference</b>
          </Box>
        </Flex>
      </Box>
    </>
  );
}

export default Sider;
