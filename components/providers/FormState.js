import React, { useReducer, createContext } from "react";
export const FormContext = createContext();
const FormState = (props) => {
  const intialState = {
    response: [], // {jobTitle:'Senior Javascript Developer',jobDesc:'react',jobLoc:'Bangalore'}
    requisitionDetails: [], // {owner:'John Doe', urgency:'High', openings:5, status:'Open', hiringManager:'John Doe', requestTitle:'Senior Javascript Developer'}
    interviewDetails: [], // {interviewMode:'Online', interviewDuration:'1 hr', interviewLang:'english'}
  };

  const formReducer = (state, action) => {
    switch (action.type) {
      case "ADD_RESPONSE":
        return {
          ...state,
          response: [action.payload],
        };
      case "ADD_REQUISITION":
        return {
          ...state,
          requisitionDetails: [action.payload],
        };
      case "ADD_INTERVIEW":
        return {
          ...state,
          interviewDetails: [action.payload],
        };

      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(formReducer, intialState);
  return (
    <FormContext.Provider value={{ state, dispatch }}>
      {props.children}
    </FormContext.Provider>
  );
};

export default FormState;
