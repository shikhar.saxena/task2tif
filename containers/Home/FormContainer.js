import { Alert } from "@chakra-ui/react";
import React from "react";
import Alrt from "../../components/Alrt";

import InterviewDetails from "../../components/InterviewDetails";
import JobDetails from "../../components/JobDetails";
import Requestition from "../../components/Requestition";

function FormContainer({ next, tabIndex, previous }) {
  return (
    <>
      {tabIndex === 0 ? (
        <Requestition next={next} tabIndex={tabIndex} previous={previous} />
      ) : tabIndex === 1 ? (
        <JobDetails next={next} tabIndex={tabIndex} previous={previous} />
      ) : tabIndex === 2 ? (
        <InterviewDetails next={next} tabIndex={tabIndex} previous={previous} />
      ) : (
        <Alrt />
      )}
    </>
  );
}

export default FormContainer;
